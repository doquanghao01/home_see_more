import 'package:flutter/material.dart';
import 'package:home_see_more/home_see_more/model/guides.dart';
import 'package:home_see_more/home_see_more/model/tours.dart';

class HomeSeeMore extends StatelessWidget {
  const HomeSeeMore({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Container(
                  height: 200,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/670301139 1.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Container(
                    color: Colors.black.withOpacity(0.3),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 44, left: 12),
                          child: InkWell(
                            onTap: () {},
                            child: const Icon(
                              Icons.arrow_back_ios,
                              size: 24,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(top: 10, left: 16),
                          child: Text(
                            "Book your own private local Guide and explore the city",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 16,
                right: 16,
                bottom: 0,
                child: Container(
                  padding: const EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width * 1,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 5,
                        blurRadius: 5,
                        offset: const Offset(
                          0,
                          1,
                        ), // changes position of shadow
                      ),
                    ],
                  ),
                  child: const Center(
                    child: TextField(
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.search,
                          color: Color.fromARGB(255, 126, 126, 126),
                        ),
                        hintText: "Hi, where do you want to explore? ",
                        border: InputBorder.none,
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: PageView(
                children: [
                  const GuidesMore(),
                  const ToursMore(),
                  Container(),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 30, top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                CircleAvatar(
                  radius: 2,
                  backgroundColor: Color(0xFFDBDBDB),
                ),
                SizedBox(
                  width: 5,
                ),
                CircleAvatar(
                  radius: 3,
                  backgroundColor: Color(0xFFDBDBDB),
                ),
                SizedBox(
                  width: 5,
                ),
                CircleAvatar(
                  radius: 4,
                  backgroundColor: Color(0xFFDBDBDB),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ToursMore extends StatelessWidget {
  const ToursMore({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listTours.length,
      itemBuilder: (context, index) => Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black.withOpacity(0.3),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(10),
                topLeft: Radius.circular(10),
              ),
              child: Stack(
                children: [
                  Image.asset(listTours[index].img!),
                  Positioned(
                    right: 10,
                    left: 10,
                    bottom: 10,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Wrap(
                          children: List.generate(
                            5,
                            (index) => Icon(
                              Icons.star,
                              size: 14,
                              color: index < listTours[index].danhgia!
                                  ? const Color(0xFFFFC100)
                                  : const Color(0xFFAFAFAF),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          "${listTours[index].slReviews!} likes",
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        listTours[index].name!,
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                      const Spacer(),
                      const Icon(
                        Icons.favorite,
                        color: Color(0xFF00CEA6),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(
                        Icons.calendar_month_outlined,
                        color: Color(0xFF555555),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        listTours[index].ngay!,
                        style: const TextStyle(
                          color: Color(0xFF555555),
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(
                        Icons.access_time_outlined,
                        color: Color(0xFF555555),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        "${listTours[index].soNgay} days",
                        style: const TextStyle(
                          color: Color(0xFF555555),
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ),
                      ),
                      const Spacer(),
                      Text(
                        "\$  ${listTours[index].giaTien}",
                        style: const TextStyle(
                          color: Color(0xFF00CEA6),
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GuidesMore extends StatelessWidget {
  const GuidesMore({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      primary: false,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisExtent: 220,
      ),
      itemCount: listGuides.length,
      itemBuilder: (BuildContext context, int index) {
        return SizedBox(
          child: Container(
            padding: const EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Stack(
                    children: [
                      Image.asset(listGuides[index].img!),
                      Positioned(
                        right: 10,
                        left: 10,
                        bottom: 10,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Wrap(
                              children: List.generate(
                                5,
                                (index) => Icon(
                                  Icons.star,
                                  size: 14,
                                  color: index < listGuides[index].danhgia!
                                      ? const Color(0xFFFFC100)
                                      : const Color(0xFFAFAFAF),
                                ),
                              ),
                            ),
                            Text(
                              "${listGuides[index].slReviews!}Reviews",
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  listGuides[index].name!,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      const Icon(
                        Icons.location_on_sharp,
                        color: Color(0xFF00CEA6),
                        size: 24,
                      ),
                      Expanded(
                        child: Text(
                          listGuides[index].address!,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: Color(0xFF00CEA6),
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
